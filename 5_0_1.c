#include <stdio.h>

int main() {
    // Input
    int quantity;
    float pricePerItem;

    // Step 1: Input Quantity
    printf("Enter quantity: ");
    scanf("%d", &quantity);

    // Step 2: Input Price Per Item
    printf("Enter price per item: ");
    scanf("%f", &pricePerItem);

    // Step 3: Calculate Total Expenses
    float totalExpenses;
    if (quantity > 1000) {
        totalExpenses = quantity * pricePerItem * 0.9;  // 10% discount
    } else {
        totalExpenses = quantity * pricePerItem;
    }

    // Step 4: Display Results
    printf("Total Expenses: %.2f\n", totalExpenses);
    printf("Dhairya Shah (23DCS118)!\n");

    // Step 5: End
    return 0;
}
