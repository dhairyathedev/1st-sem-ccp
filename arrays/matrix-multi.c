#include<stdio.h>

int main(){
  int a[3][3],
      b[3][3],
      c[3][3];

  printf("Enter the elements of the first matrix: \n");
  for(int i = 0; i < 3; i++){
    for(int j = 0; j < 3; j++){
      printf("a[%d][%d]: ", i,j);
      scanf("%d", &a[i][j]);
    }
  }
  printf("\nEnter the elemnts of the second matrix: \n");
  for(int i = 0; i < 3; i++){
    for(int j = 0; j < 3; j++){
      printf("b[%d][%d]: ", i,j);
      scanf("%d", &b[i][j]);
    }
  }

  printf("\nThe Matrix Adition is: \n");
  for(int i = 0; i < 3; i++){
    for(int j = 0; j < 3; j++){
      c[i][j] = a[i][j] * b[i][j];
    }
  }

  printf("\nThe resuletant matrix is: \n");
  for(int i = 0; i < 3; i++){
    printf("\n");
    
    for(int j = 0; j < 3; j++){  
      printf("%d ", c[i][j]);
    }
  }
}