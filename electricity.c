#include<stdio.h>

int main(){
	int unit, bill;
	printf("Enter your electricity units: ");
	scanf("%d", &unit);
	
	if(unit > 50 && unit < 100){
		bill = unit * 5;
	}else if(unit > 100 && unit < 250){
		bill = unit * 10;
	}else if(unit > 250){
		bill = unit * 15;
	}else{
		bill = 0;
	}
	printf("Your bill is Rs.%d", bill);
	printf("\n");
	return 0;
}
