#include <stdio.h>

int main()
{
    int totalStick = 21;
    while (totalStick > 0)
    {
        int user_pick = 0;
        printf("Enter 1, 2, 3, or 4 match-sticks: ");
        scanf("%d", &user_pick);
        if (user_pick < 1 || user_pick > 4 || user_pick > totalStick)
        {
            printf("Invalid input. Please enter a valid number.\n");
            continue;
        }

        totalStick -= user_pick;
        printf("You have picked %d\n", user_pick);
        printf("Computer has picked %d\n", 5 - user_pick);
        printf("Total Sticks Remaining %d\n", totalStick);
        if (totalStick == 0)
        {
            printf("You picked the last match-stick. You lose!\n");
            break;
        }
    }
    printf("Dhairya Shah (23DCS118)\n");
    return 0;
}