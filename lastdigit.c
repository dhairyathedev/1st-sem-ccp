#include<stdio.h>

int main(){
	int n;
	printf("Enter a four digit number: ");
	scanf("%d", &n);
	if(n > 9999){
		printf("Please enter a four digit number");
	}else{
		printf("Last digit of %d is %d", n, n % 10);
	}
	printf("\n");
	return 0;
}

