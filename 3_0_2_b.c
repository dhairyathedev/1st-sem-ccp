#include <stdio.h>

int main() {
    // Initial count of Yellow and Pink balls
    int Yellow = 10;
    int Pink = 20;

    // Instructions
    int calculate = ++Yellow + Yellow++ + --Yellow + ++Pink - --Pink - --Pink;

    // Display Results
    printf("Sr. No.\tInstructions\t\tYellow\tPink\n");
    printf("1.\tCount before execution\t%d\t%d\n", 10, 20);
    printf("2.\tCount after execution\t%d\t%d\n", Yellow, Pink);
    printf("3.\tCount of calculate\t%d\n", calculate);
    printf("Dhairya Shah (23DCS118)!\n");

    return 0;
}
