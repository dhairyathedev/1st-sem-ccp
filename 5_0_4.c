#include <stdio.h>
#include <math.h>

int main() {
    // Input
    float a, b, c;

    // Step 1: Input coefficients a, b, and c
    printf("Enter coefficients a, b, and c: ");
    scanf("%f %f %f", &a, &b, &c);

    // Step 2: Calculate Discriminant
    float discriminant = b * b - 4 * a * c;

    // Step 3: Nested Switch Case
    switch (discriminant > 0) {
        case 1:
            // Discriminant > 0
            switch (1) {
                case 1:
                    // Calculate and print root1 and root2
                    printf("Root1 = %.2f\n", (-b + sqrt(discriminant)) / (2 * a));
                    printf("Root2 = %.2f\n", (-b - sqrt(discriminant)) / (2 * a));
                    break;
            }
            break;

        case 0:
            // Discriminant <= 0
            switch (discriminant < 0) {
                case 1:
                    // Calculate and print root1 and root2 with imaginary part
                    printf("Root1 = %.2f + %.2fi\n", -b / (2 * a), sqrt(-discriminant) / (2 * a));
                    printf("Root2 = %.2f - %.2fi\n", -b / (2 * a), sqrt(-discriminant) / (2 * a));
                    break;

                case 0:
                    // Discriminant = 0
                    switch (discriminant == 0) {
                        case 1:
                            // Calculate and print root1 and root2
                            printf("Root1 = Root2 = %.2f\n", -b / (2 * a));
                            break;
                    }
                    break;
            }
            break;
    }
    printf("Dhairya Shah (23DCS118)!\n");
    // Step 4: End
    return 0;
}
