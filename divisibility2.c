#include<stdio.h>

int main(){
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);
	if(n % 25 == 0){
		printf("%d is divisible by 25.", n);	
	}else{
		printf("%d is not divisible by 25.", n);
	}
	printf("\n");
	return 0;
}
