#include <stdio.h>
#include <math.h>

int isPrime(int num) {
    if (num < 2) {
        return 0; // Not a prime number
    }

    for (int i = 2; i <= num / 2; ++i) {
        if (num % i == 0) {
            return 0; // Not a prime number
        }
    }

    return 1; // Prime number
}

int isArmstrong(int num) {
    int originalNum, remainder, result = 0, n = 0;

    originalNum = num;

    while (originalNum != 0) {
        originalNum /= 10;
        ++n;
    }

    originalNum = num;

    while (originalNum != 0) {
        remainder = originalNum % 10;
        result += pow(remainder, n);
        originalNum /= 10;
    }

    return (result == num);
}

int isPerfect(int num) {
    int sum = 0;

    for (int i = 1; i <= num / 2; ++i) {
        if (num % i == 0) {
            sum += i;
        }
    }

    return (sum == num);
}

int main() {
    int choice, num;

    do {
        printf("Menu:\n");
        printf("1. Prime or not\n");
        printf("2. Armstrong number or not\n");
        printf("3. Perfect number or not\n");
        printf("4. Exit\n");

        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printf("Enter a number: ");
                scanf("%d", &num);
                if (isPrime(num)) {
                    printf("%d is a prime number.\n", num);
                } else {
                    printf("%d is not a prime number.\n", num);
                }
                break;

            case 2:
                printf("Enter a number: ");
                scanf("%d", &num);
                if (isArmstrong(num)) {
                    printf("%d is an Armstrong number.\n", num);
                } else {
                    printf("%d is not an Armstrong number.\n", num);
                }
                break;

            case 3:
                printf("Enter a number: ");
                scanf("%d", &num);
                if (isPerfect(num)) {
                    printf("%d is a perfect number.\n", num);
                } else {
                    printf("%d is not a perfect number.\n", num);
                }
                break;

            case 4:
                printf("Exiting the program.\n");
                break;

            default:
                printf("Invalid choice. Please enter a number between 1 and 4.\n");
        }

    } while (choice != 4);

    return 0;
}
