#include <stdio.h>


void copyString(char destination[], const char string1[], const char string2[]){
  int i = 0;
  int j = 0;

  while(string1[i] != '\0'){
    destination[i] = string1[i];
    i++;
  }

  while(string2[j] != '\0'){
    destination[i] = string2[j];
    i++;
    j++;
  }

  destination[i] = '\0';
  
}

int main(){
  char string1[] = "Hello";
  char string2[] = " World!";
  char copied[100];

  copyString(copied, string1, string2);
  printf("%s", copied);
  return 0;
}