#include <stdio.h>

int main() {
    char characteristics[50];

    // Input using gets
    printf("Enter your characteristics (not more than 50 words): ");
    gets(characteristics);

    // Output using puts
    printf("Your characteristics: ");
    puts(characteristics);
    printf("Dhairya Shah (23DCS118)!\n");

    return 0;
}
