#include<stdio.h>

int main(){
	float unitCost = 350.00, totalCost;
	int quantity;
	
	printf("Enter a quantity: ");
	scanf("%d", &quantity);
			
	totalCost = quantity * unitCost;

	if(totalCost > 4000){
		totalCost -= 0.10 * totalCost;
		printf("You have got 10%% discount.\n");
	}
	printf("Total cost: Rs.%0.2f\n", totalCost);
	return 0;
}
