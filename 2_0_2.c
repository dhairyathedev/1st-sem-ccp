#include <stdio.h>

int main() {
    // Input
    float basicSalary, DA, HRA, MA, TA, PF, IT, grossSalary, netSalary;

    printf("Enter your Basic Salary: ");
    scanf("%f", &basicSalary);

    // Calculate Allowances
    DA = 0.7 * basicSalary;
    HRA = 0.07 * basicSalary;
    MA = 0.02 * basicSalary;
    TA = 0.04 * basicSalary;

    // Calculate Deductions
    PF = 0.12 * basicSalary;
    printf("Enter Income Tax (IT): ");
    scanf("%f", &IT);

    // Calculate Gross Salary
    grossSalary = basicSalary + DA + HRA + MA + TA;

    // Calculate Net Salary
    netSalary = grossSalary - (PF + IT);

    // Display Results
    printf("\nSr. No.\t\tInput/Outputs\t\t\tAmount\n");
    printf("1\t\tEnter your Basic Salary\t\t%.2f\n", basicSalary);
    printf("2\t\tDA of Basic Salary\t\t%.2f\n", DA);
    printf("3\t\tHRA of Basic Salary\t\t%.2f\n", HRA);
    printf("4\t\tMA of Basic Salary\t\t%.2f\n", MA);
    printf("5\t\tTA of Basic Salary\t\t%.2f\n", TA);
    printf("6\t\tPF of Basic Salary\t\t%.2f\n", PF);
    printf("7\t\tGross Salary\t\t\t%.2f\n", grossSalary);
    printf("8\t\tNet Salary\t\t\t%.2f\n", netSalary);
    printf("Dhairya Shah (23DCS118)!\n");
    return 0;
}
