#include <stdio.h>
#include <stdlib.h>

int main()
{
	int days;
	printf("Enter the number of days: ");
	scanf("%d", &days);
	int years = days / 365;
	int remainingDays = days % 365;
	int weeks = remainingDays / 7;
	int month = weeks / 4;

	printf("%d days is approximately %d years and %d months or %d weeks.\n", days, years,month, weeks);
}
