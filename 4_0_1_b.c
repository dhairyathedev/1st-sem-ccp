#include <stdio.h>

int main() {
    int decimalNumber;

    // Input
    printf("Enter a decimal number: ");
    scanf("%d", &decimalNumber);

    // Output
    printf("Sr. No.\tInputs\t\tOctal\tHexadecimal\n");
    printf("1.\tYour Roll No\t%o\t%x\n", decimalNumber, decimalNumber);
    printf("2.\t143\t\t%o\t%x\n", 143, 143);
    printf("3.\t0\t\t%o\t%x\n", 0, 0);
    printf("4.\t1\t\t%o\t%x\n", 1, 1);
    printf("5.\t-1\t\t%o\t%x\n", -1, -1);
    printf("Dhairya Shah (23DCS118)!\n");

    return 0;
}
