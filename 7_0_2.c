#include<stdio.h>

int main(){
	int arr1[5] = {5, 3, 4, 1, 2};
	int arr2[5] = {9, 6, 8, 7, 10};
	int new_arr[10];

	for(int i = 0; i < 5; i++){
		new_arr[i] = arr1[i];
	}

	for(int j = 5, k = 0; j < 10; j++ ,k++){
		new_arr[j] = arr2[k];
	} 

	for (int i = 0; i < 10; i++){
		for(int j = i + 1; j < 10; j++){
			if(new_arr[i] > new_arr[j]){
				int a = new_arr[i];
				new_arr[i] = new_arr[j];
				new_arr[j] = a;
			}
		}
	}
	
	// Printing the new_arr
	for(int k = 0; k < 10; k++){
		printf("%d ", new_arr[k]);
	}
}
