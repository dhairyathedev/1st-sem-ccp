#include <stdio.h>
#include <math.h>

int main() {
    // Input
    int x1, y1, x2, y2, x3, y3;

    // Step 1: Input Points
    printf("Enter coordinates of three points (x1 y1 x2 y2 x3 y3): ");
    scanf("%d %d %d %d %d %d", &x1, &y1, &x2, &y2, &x3, &y3);

    // Step 2: Calculate Slopes
    float s1 = fabs((float)(x2 - x1) / (y2 - y1));
    float s2 = fabs((float)(x3 - x2) / (y3 - y2));
    float s3 = fabs((float)(x3 - x1) / (y3 - y1));

    // Step 3: Check Collinearity
    if (s1 == s2 && s2 == s3) {
        printf("Collinear Points\n");
    } else {
        printf("Non-collinear Points\n");
    }
    printf("Dhairya Shah (23DCS118)!\n");

    // Step 4: End
    return 0;
}
