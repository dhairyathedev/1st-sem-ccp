#include <stdio.h>
#include <ctype.h>

int main() {
    // Input
    char character;

    // Step 1: Input a Character
    printf("Enter a character: ");
    character = getchar();

    // Step 2: Character Test
    int isDigit = isdigit(character);
    int isAlpha = isalpha(character);
    int isLower = islower(character);
    int isPrint = isprint(character);
    int isPunct = ispunct(character);
    int isSpace = isspace(character);
    int isUpper = isupper(character);

    // Step 3: Convert Case
    char upperCase = toupper(character);
    char lowerCase = tolower(character);

    // Step 4: Display Results
    printf("\nOriginal Character: %c\n", character);

    if (isDigit)
        printf("%c is a digit\n", character);

    if (isAlpha) {
        printf("%c is %s case character , ", character, (isLower ? "lower" : "upper"));
        printf("Opposite Case Character is %c.\n", (isLower ? upperCase : lowerCase));
    }

    if (isPunct)
        printf("%c is a punctuation mark\n", character);

    if (!isPrint)
        printf("%c is a non printable character.\n", character);

    if (isSpace)
        printf("%c is a white space.\n", character);

    printf("Dhairya Shah (23DCS118)!\n");
    // Step 5: End
    return 0;
}
