#include <stdio.h>
#include <math.h>

#define PI 3.14

int main() {
    // Input
    float L, G, T;

    // Step 1: Input Length
    printf("Enter the length of the pendulum (in meters): ");
    scanf("%f", &L);

    // Step 2: Input Gravity
    printf("Enter the acceleration due to gravity (in m/s^2): ");
    scanf("%f", &G);

    // Step 3: Calculate Time Period
    T = 2 * PI * sqrt(L / G);

    // Step 4: Display Results
    printf("\nSr. No.\tInput\t\t\t\t\t\tOutput\n");
    printf("1.\tLength: %.2f m, Gravity: %.2f m/s^2\t\tTime Calculated: %.2f seconds\n", L, G, T);
    printf("Dhairya Shah (23DCS118)!\n");
    // Step 5: End
    return 0;
}
