#include<stdio.h>

int main(){
	int n;
	printf("Enter a four digit number: ");
	scanf("%d", &n);
	
	if(n > 9999){
		printf("Number exceeded the limit.");
	}else{
		if((n % 10) % 9 == 0){
			printf("The last digit is divisible by 9.");
		}else{
			printf("The last digit is not divisible by 9.");
		}
	}
	printf("\n");
	return 0;
}
