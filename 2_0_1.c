#include <stdio.h>

int main() {
    // Input
    int totalPopulation = 80000;
    float literacyPercentage = 0.48;
    float menPercentage = 0.52;
    float literateMenPercentage = 0.35;

    // Calculations
    int totalLiteracy = totalPopulation * literacyPercentage;
    int numMen = totalPopulation * menPercentage;
    int numLiterateMen = totalPopulation * literateMenPercentage;
    int numIlliterateMen = numMen - numLiterateMen;
    int numWomen = totalPopulation - numMen;
    int numLiterateWomen = totalLiteracy - numLiterateMen;
    int numIlliterateWomen = numWomen - numLiterateWomen;

    // Display Results
    printf("Sr. No\t\tGet Outcome\t\t\t\t\tValue\n");
    printf("1\t\tTotal Population\t\t\t\t%d\n", totalPopulation);
    printf("2\t\tNumber of Literate (Men + Women)\t\t%d\n", totalLiteracy);
    printf("3\t\tNumber of Men\t\t\t\t\t%d\n", numMen);
    printf("4\t\tNumber of Literate Men\t\t\t\t%d\n", numLiterateMen);
    printf("5\t\tNumber of Illiterate Men\t\t\t%d\n", numIlliterateMen);
    printf("6\t\tNumber of Women\t\t\t\t\t%d\n", numWomen);
    printf("7\t\tNumber of Literate Women\t\t\t%d\n", numLiterateWomen);
    printf("8\t\tNumber of Illiterate Women\t\t\t%d\n", numIlliterateWomen);
    printf("Dhairya Shah (23DCS118)!\n");
    return 0;
}
