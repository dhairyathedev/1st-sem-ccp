#include <stdio.h>

int main() {
    // Initial count of Yellow balls
    int Yellow = 10;

    // Instructions
    // i. Rajiv: ++Yellow
    ++Yellow;

    // ii. Preet: --Yellow
    --Yellow;

    // iii. Raj: Yellow++
    Yellow++;

    // iv. Ritul: Yellow--
    Yellow--;

    // Display Results
    printf("Sr. No.\tInstructions\t\tYellow\n");
    printf("1.\tCount before execution\t%d\n", 10);
    printf("2.\tCount after execution\t%d\n", Yellow);
    printf("Dhairya Shah (23DCS118)!\n");

    return 0;
}
