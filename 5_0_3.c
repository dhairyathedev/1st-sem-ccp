#include <stdio.h>

int main()
{
    // Input
    int year;

    // Step 1: Input Year
    printf("Enter a year: ");
    scanf("%d", &year);

    // Step 2: Check Leap Year
    if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
    {
        printf("%d is a Leap Year\n", year);
    }
    else
    {
        printf("%d is not a Leap Year\n", year);
    }
    printf("Dhairya Shah (23DCS118)!\n");

    // Step 3: End
    return 0;
}
