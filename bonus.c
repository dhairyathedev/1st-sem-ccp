#include<stdio.h>
#include<time.h>
int main(){
	int salary, year;
	float netBonus;
	time_t currentTime;
	struct tm *timeInfo;
	
	time(&currentTime);
	timeInfo = localtime(&currentTime);
	
	int curYear= timeInfo->tm_year + 1900;
	// printf("%d", curYear);
	
	printf("Enter the year of joining: ");
	scanf("%d", &year);
	
	if(year > curYear){
		printf("Please enter the year before %d", curYear);
	}else{
		printf("Enter your salary for 15%% bonus: ");
		scanf("%d", &salary);

		if(curYear - year > 7){
			netBonus = 0.15 * salary;
		}else{
			netBonus = 0;
		}
		printf("The net bonus of the employee is: Rs.%0.2f", netBonus);
}
	printf("\n");
	return 0;
}
