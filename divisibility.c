#include<stdio.h>

int main(){
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);
	if(n % 11 == 0){
		printf("%d is divisible by 11", n);
	}else if(n % 13 == 0){
		printf("%d is divisible by 13", n);
	}else{
		printf("%d is neither divisible by 11 nor 13", n);	
	}
	printf("\n");
	return 0;
}
