#include <stdio.h>

int main()
{
    int n;
    printf("Please enter a number: ");
    scanf("%d", &n);
    int odd=0, even=0;
    for (int i = 1; i < n; i++)
    {
        (i % 2 == 0) ? (printf("%d is even.\n", i), even++) : (printf("%d is odd.\n", i), odd++);
    }
    printf("%d numbers are odd\n", odd);
    printf("%d numbers are even\n", even);
    printf("%.1f is the average\n", (float)((odd+even)/2));
}